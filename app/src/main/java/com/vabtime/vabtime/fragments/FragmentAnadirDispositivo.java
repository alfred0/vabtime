package com.vabtime.vabtime.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.vabtime.vabtime.R;
import com.vabtime.vabtime.model.Device;
import com.vabtime.vabtime.model.IpAddress;
import com.vabtime.vabtime.sqlite.VabtimeDataBase;

import java.io.IOException;
import java.util.ArrayList;

public class FragmentAnadirDispositivo extends Fragment implements View.OnClickListener {
    private View view;
    private CameraSource cameraSource;
    private SurfaceView cameraView;
    private final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private String token = "";
    private String tokenanterior = "";
    private Button button_anadir_dispositivo;
    private VabtimeDataBase vabtimeDataBase;
    private EditText dirIP;
    private Device device;
    private ArrayList<Device> devices;

    public FragmentAnadirDispositivo() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_fragment_anadir_dispositivo, container, false);
        cameraView = (SurfaceView) view.findViewById(R.id.camera_view);
        button_anadir_dispositivo=(Button)view.findViewById(R.id.button_anadir_dispositivo);
        dirIP=(EditText)view.findViewById(R.id.dirIP);
        device=new Device();
        button_anadir_dispositivo.setOnClickListener(this);

        devices=new ArrayList<Device>();
        vabtimeDataBase= new VabtimeDataBase(getContext());
        devices=vabtimeDataBase.getDevices();
        Log.i("size-arraylist","size"+devices.size());

        initQR();
        return view;
        //http://www.desarrollolibre.net/blog/tema/267/android/como-crear-un-lector-de-codigos-qr-en-android-con-android-studio#.WpN-9ajwbIV
    }
    public void initQR(){
        // creo el detector qr
        BarcodeDetector barcodeDetector =
                new BarcodeDetector.Builder(getContext())
                        .setBarcodeFormats(Barcode.ALL_FORMATS)
                        .build();
        // creo la camara
        cameraSource = new CameraSource
                .Builder(getContext(), barcodeDetector)
                .setRequestedPreviewSize(1600, 1024)
                .setAutoFocusEnabled(true) //you should add this feature
                .build();

        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                // verifico si el usuario dio los permisos para la camara
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        // verificamos la version de ANdroid que sea al menos la M para mostrar
                        // el dialog de la solicitud de la camara
                        if (shouldShowRequestPermissionRationale(
                                Manifest.permission.CAMERA)) ;
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                    }
                    return;
                } else {
                    try {
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException ie) {
                        Log.e("CAMERA SOURCE", ie.getMessage());
                    }
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });
        // preparo el detector de QR
        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() > 0) {

                    // obtenemos el token
                    token = barcodes.valueAt(0).displayValue.toString();

                    // verificamos que el token anterior no se igual al actual
                    // esto es util para evitar multiples llamadas empleando el mismo token
                    if (!token.equals(tokenanterior)) {

                        // guardamos el ultimo token proceado
                        tokenanterior = token;
                        Log.i("token", token);

                        if (URLUtil.isValidUrl(token)) {
                            // si es una URL valida abre el navegador
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(token));
                            startActivity(browserIntent);
                        } else {
                            // comparte en otras apps
                            Intent shareIntent = new Intent();
                            shareIntent.setAction(Intent.ACTION_SEND);
                            shareIntent.putExtra(Intent.EXTRA_TEXT, token);
                            shareIntent.setType("text/plain");
                            startActivity(shareIntent);
                        }
                        dirIP.setText(token.toString());

                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    synchronized (this) {
                                        wait(5000);
                                        // limpiamos el token
                                        tokenanterior = "";
                                    }
                                } catch (InterruptedException e) {
                                    // TODO Auto-generated catch block
                                    Log.e("Error", "Waiting didnt work!!");
                                    e.printStackTrace();
                                }
                            }
                        }).start();

                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_anadir_dispositivo:
                if(dirIP.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Ingrese dirección IP", Toast.LENGTH_SHORT).show();
                }else{
                    //validar si ya se eligio algun codigo qr
                    device.setId("ID001");
                    device.setIpAdress(dirIP.getText().toString());
                    device.setName("ddddedd");
                    vabtimeDataBase= new VabtimeDataBase(getContext());
                    vabtimeDataBase.newDevice(device);
                    IpAddress ipAddress =new IpAddress(dirIP.getText().toString(),getContext());
                    ipAddress.saveiIpAddress();
                    Toast.makeText(getContext(), "Has añadido un dispositivo", Toast.LENGTH_SHORT).show();
                }
                //guardar en sqlite
                break;
        }
    }
}
