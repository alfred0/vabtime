package com.vabtime.vabtime.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vabtime.vabtime.R;

public class LoginVabtime extends AppCompatActivity implements View.OnClickListener {
    private EditText edtUsername,edtPassword;
    private Button buttonLogin;
    public static final String CAMPOS_VACIOS = "Campos incompletos";
    public static final String USERNAME = "admin";
    public static final String PASSWORD = "admin235";
    public static final String DATOS_INCORRECTOS = "Campos incompletos";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_vabtime);
        edtUsername=(EditText)findViewById(R.id.edtUsername);
        edtPassword=(EditText)findViewById(R.id.edtPassword);
        buttonLogin=(Button)findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonLogin:
                if(edtUsername.getText().toString().equals("") || edtPassword.getText().toString().equals("")){
                    Toast.makeText(this, CAMPOS_VACIOS, Toast.LENGTH_SHORT).show();}
                else{
                        if(!edtUsername.getText().toString().equals(USERNAME)|| !edtPassword.getText().toString().equals(PASSWORD)){
                            Toast.makeText(this, DATOS_INCORRECTOS, Toast.LENGTH_SHORT).show();
                        }else{
                            Intent intent=new Intent(LoginVabtime.this,MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                break;
        }
    }
}
