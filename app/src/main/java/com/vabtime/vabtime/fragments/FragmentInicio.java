package com.vabtime.vabtime.fragments;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.vabtime.vabtime.R;
import com.vabtime.vabtime.TareasSegundoPlano.HttpGetRequest;
import com.vabtime.vabtime.model.Alarm;
import com.vabtime.vabtime.model.Datas;
import com.vabtime.vabtime.model.Device;
import com.vabtime.vabtime.model.IdDevice;
import com.vabtime.vabtime.model.IpAddress;
import com.vabtime.vabtime.model.Switch;
import com.vabtime.vabtime.model.Time;
import com.vabtime.vabtime.notification.AlertReceiver;
import com.vabtime.vabtime.sqlite.VabtimeDataBase;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import io.ghyeok.stickyswitch.widget.StickySwitch;


public class FragmentInicio extends Fragment implements View.OnClickListener {
    private View view;
    private TextView hora;
    private Button button_establecer_hora;
    private static final String DIALOG_TIME = "MainActivity.TimeDialog";
    private StickySwitch stickySwitch;
    private ImageView imgDevice;

    //Some url endpoint that you may have
    private String urlServiceON = "http://";//Define boiler status as ON
    private String urlServiceON2 = "&st=1";//Define boiler status as ON

    private String urlServiceOFF = "http://";//Define boiler status as OFF
    private String urlServiceOFF2 = "&st=0";//Define boiler status as OFF
    //String to place our result in
    private String result;

    private Calendar time=Calendar.getInstance();
    private DateFormat format=DateFormat.getDateTimeInstance();

    //status of StickySwitch
    private String STATUSON="On";
    private String STATUSOFF="Off";
    private String STATUS;
    private String ALARM;
    private String IPADDRESS;
    private MaterialSpinner spinner;


    //
    private ArrayList<Device> devices;
    private VabtimeDataBase vabtimeDataBase;

    public FragmentInicio() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_fragment_inicio, container, false);
        button_establecer_hora=(Button)view.findViewById(R.id.button_establecer_hora);
        hora=(TextView)view.findViewById(R.id.hora);
        button_establecer_hora.setOnClickListener(this);
        stickySwitch=(StickySwitch)view.findViewById(R.id.stick_switch);
        imgDevice=(ImageView)view.findViewById(R.id.imgDevice);

        devices=new ArrayList<Device>();
        vabtimeDataBase= new VabtimeDataBase(getContext());
        devices=vabtimeDataBase.getDevices();

        Alarm alarm =new Alarm(getContext());
        ALARM=alarm.getAlarm();

        if(ALARM.equals(""))
            hora.setText("No hay alarma");
        else
            hora.setText(ALARM);

        Switch sw =new Switch(getContext());
        STATUS=sw.getStatus();
        if(STATUS.equals(STATUSON)){
            stickySwitch.setDirection(StickySwitch.Direction.RIGHT);
            stickySwitch.setSliderBackgroundColor(0xFF00897b);
            stickySwitch.setSwitchColor(0xFF353E4F);
        }else {
            stickySwitch.setDirection(StickySwitch.Direction.LEFT);
            stickySwitch.setSliderBackgroundColor(0xFF9E9E9E);
            stickySwitch.setSwitchColor(0xFF424242);
        }


        stickySwitch.setOnSelectedChangeListener(new StickySwitch.OnSelectedChangeListener() {
            @Override
            public void onSelectedChange(StickySwitch.Direction direction, String s) {
                //obtener ip del dispositivo seleccionado y mandarlo como parametro
                if(spinner.getSelectedIndex()!=0){
                    encenderApagarSwitch(s);
                }else
                    Toast.makeText(getContext(), "Seleccione un dispositivo", Toast.LENGTH_SHORT).show();

            }
        });

        spinner = (MaterialSpinner) view.findViewById(R.id.spinner);
        //ArrayAdapter arrayAdapter= new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item, (List) devices.get(0));
        spinner.setAdapter(getArrayAdapterFromArrayListForSpinner(devices));
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(spinner.getSelectedIndex()!=1)
                    imgDevice.setImageResource(R.drawable.idea);
                else
                    imgDevice.setImageResource(R.drawable.boiler);

                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
            }
        });

        return view;
    }

    /**metodo para obtener el id del dispositivo desde el spinner*/
    public String getIdDevice(){
        String id = null;
        for (int i = 0; i < devices.size(); i++)
        {
            if(i+1==spinner.getSelectedIndex())
                return devices.get(i).getId();
        }
        return id;
    }

    /**metodo para obtner la ip de dispositivos pasandole si id*/
    public String getIPADDRESS(String id){
        return vabtimeDataBase.getIpAddress(id);
    }
    /**Método para crear adapter del spinner*/
    public ArrayAdapter<String> getArrayAdapterFromArrayListForSpinner(ArrayList<Device> devices)
    {
        ArrayList<String> devide=new ArrayList<String>();
        devide.add("Seleccione un dispositivo");
        try {
            for (int i = 0; i < devices.size(); i++)
            {
                devide.add(devices.get(i).getName());
                Log.i("device","get Name "+devices.get(i).getName());
                Log.i("device","get Id "+devices.get(i).getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayAdapter<String> aArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,devide);
        return aArrayAdapter;
    }

    private void encenderApagarSwitch(String s) {

            try {
                if(s.equals(STATUSON)) {
                    Switch sw =new Switch(STATUSON,getContext());
                    sw.saveStatus();
                    stickySwitch.setSliderBackgroundColor(0xFF00897b);
                    stickySwitch.setSwitchColor(0xFF353E4F);
                    Log.i("device","get IpAddress "+getIdDevice());
                    serviceGetOn(getIPADDRESS(getIdDevice()),getIdDevice());
                }
                if(s.equals(STATUSOFF)) {
                    Switch sw =new Switch(STATUSOFF,getContext());
                    sw.saveStatus();
                    stickySwitch.setSliderBackgroundColor(0xFF9E9E9E);
                    stickySwitch.setSwitchColor(0xFF424242);
                    Log.i("device","get IpAddress "+getIdDevice());
                    serviceGetOff(getIPADDRESS(getIdDevice()),getIdDevice());
                }
            }catch (Exception e){
                e.getMessage();
            }


    }

    public void startAlarm(){
        Intent intent=new Intent(getContext(), AlertReceiver.class);
        PendingIntent pendingIntent=PendingIntent.getBroadcast(getContext(),AlertReceiver.REQUEST_CODE,intent,0);
        long firtsMillis=System.currentTimeMillis();
        int intervalMillis=1*3*1000;
        AlarmManager alarmManager=(AlarmManager)getContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,firtsMillis,intervalMillis,pendingIntent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_establecer_hora:
                    new TimePickerDialog(getContext(), t, time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), true).show();
                break;
        }
    }
    public void serviceGetOn(String IPADD,String device) throws ExecutionException, InterruptedException {
        HttpGetRequest getRequest = new HttpGetRequest();
        //Perform the doInBackground method, passing in our url
        result = getRequest.execute(urlServiceON+IPADD+"/?d="+device+urlServiceON2).get();
        Log.i("","serviceGetOn"+urlServiceON+IPADD+"/?d="+device+urlServiceON2);
    }
    public void serviceGetOff(String IPADD,String device) throws ExecutionException, InterruptedException {
        HttpGetRequest getRequest = new HttpGetRequest();
        //Perform the doInBackground method, passing in our url
        result = getRequest.execute(urlServiceOFF+IPADD+"/?d="+device+urlServiceOFF2).get();
        Log.i("","serviceGetOff"+urlServiceOFF+IPADD+"/?d="+device+urlServiceOFF2);
    }

    TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hourofday, int minute) {
                time.set(Calendar.HOUR_OF_DAY, hourofday);
                time.set(Calendar.MINUTE, minute);
                time.set(Calendar.SECOND, 0);

                updateTextLabel();
                Time timee = new Time();
                timee.setHour(String.valueOf(hourofday));
                timee.setMinute(String.valueOf(minute));

                Datas datos = new Datas(timee, getContext());
                datos.saveTime();

                Alarm alarm = new Alarm(format.format(time.getTime()), getContext());
                alarm.saveAlarm();

                IpAddress ipAddress =new IpAddress(getIPADDRESS(getIdDevice()),getContext());
                ipAddress.saveiIpAddress();

                IdDevice idDevice=new IdDevice(getIdDevice(),getContext());
                idDevice.saveIdDevice();

                startAlarm();
        }
    };

    public void updateTextLabel(){
        hora.setText(format.format(time.getTime()));
    }

    @Override
    public void onResume() {
        super.onResume();
        //seleccion de la base de datos los dispositivos
        spinner.setAdapter(getArrayAdapterFromArrayListForSpinner(devices));
        Log.i("onResume","size"+devices.size());
    }
}
