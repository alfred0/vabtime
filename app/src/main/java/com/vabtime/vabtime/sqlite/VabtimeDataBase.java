package com.vabtime.vabtime.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.vabtime.vabtime.model.Device;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;

/**
 * Created by ELVIA on 14/03/2018.
 */

public class VabtimeDataBase extends SQLiteOpenHelper {
    public static final int VERSION_BD = 1;
    public static final String NOMOBRE_BD = "vabtime.db";
    private Cursor cursor;

    public VabtimeDataBase(Context context) {
        super(context, NOMOBRE_BD, null,VERSION_BD );
    }
    @Override

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + SchemaVabtime.TABLEDEVICES.NAMETABLE + "("
                + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + SchemaVabtime.TABLEDEVICES.NAME + " TEXT, "
                + SchemaVabtime.TABLEDEVICES.IPADRESS + " TEXT, "
                + "UNIQUE (" + _ID + "))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
    /**
     * Métod for insert register on the table devices*/
    public  void newDevice(Device device){
        ContentValues valores =new ContentValues();
        valores.put(SchemaVabtime.TABLEDEVICES.NAME,device.getName());
        valores.put(SchemaVabtime.TABLEDEVICES.IPADRESS,device.getIpAdress());
        this.getWritableDatabase().insert(SchemaVabtime.TABLEDEVICES.NAMETABLE,null,valores);
    }
    /**
     * Métod for get devices*/
    public ArrayList<Device> getDevices(){
        ArrayList<Device> listDevices=new ArrayList<Device>();
        SQLiteDatabase db=this.getReadableDatabase();

        cursor=db.query(SchemaVabtime.TABLEDEVICES.NAMETABLE, null,null, null, null, null, null);
        while (cursor.moveToNext()){
            Device device=new Device();
            device.setId(cursor.getString(0));
            device.setName(cursor.getString(1));
            device.setIpAdress(cursor.getString(2));
            listDevices.add(device);
        }
        db.close();
        return listDevices;
    }
    /**
     * Métod for get ipAddress from idDevice */
    public String getIpAddress(String idDevice){
        String ip = null;
        SQLiteDatabase db=this.getReadableDatabase();
        cursor=db.query(SchemaVabtime.TABLEDEVICES.NAMETABLE, null,_ID + " LIKE ?", new String[]{idDevice}, null, null, null);
        if(cursor.moveToNext()){
            ip=cursor.getString(2);
        }
        db.close();
        return ip;
    }
}
