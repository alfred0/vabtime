package com.vabtime.vabtime.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.vabtime.vabtime.R;
import com.vabtime.vabtime.TareasSegundoPlano.HttpGetRequest;
import com.vabtime.vabtime.activities.MainActivity;
import com.vabtime.vabtime.model.Alarm;
import com.vabtime.vabtime.model.Datas;
import com.vabtime.vabtime.model.IdDevice;
import com.vabtime.vabtime.model.IpAddress;
import com.vabtime.vabtime.model.Switch;
import com.vabtime.vabtime.model.Time;

import java.util.Calendar;
import java.util.concurrent.ExecutionException;

/**
 * Created by Alfredo on 16/02/2018.
 */

public class AlertReceiver extends BroadcastReceiver {
    private NotificationManager notificationManager;
    public static final int REQUEST_CODE = 0;
    private final int NOTIFICATION_ID = 1010;
    //Some url endpoint that you may have
    private String urlServiceON = "http://";//Define boiler status as ON
    private String urlServiceON2 = "&st=1";//Define boiler status as ON
    private String urlServiceOFF = "http://192.168.0.102/?st=0";//Define boiler status as OFF
    //String to place our result in
    private String result;

    private String IPADDRESS;
    private String IDDEVICE;

    private String STATUSOFF="Off";
    private String NOTHINGALARM="No hay alarma";
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("onReceive---","onReceive---");
        Intent i = new Intent(context, MyTestService.class);
        context.startService(i);
        Calendar calendar=Calendar.getInstance();
        Time time=new Time();
        int hour, minute;
        hour=calendar.get(Calendar.HOUR_OF_DAY);
        minute=calendar.get(Calendar.MINUTE);

        IpAddress ipAddress =new IpAddress(context);
        IPADDRESS=ipAddress.getIpAddress();

        IdDevice idDevice =new IdDevice(context);
        IDDEVICE=idDevice.getIdDevice();


        Datas datos=new Datas(context);
        time=datos.getResponseTime();
        if(time.getHour().equals(hour+"")&& time.getMinute().equals(minute+"")){
            Log.i("hour-alarm---","hour-alarm---");
            HttpGetRequest getRequest = new HttpGetRequest();
            //Perform the doInBackground method, passing in our url
            try {
                Log.i("","AlertReceiver---"+urlServiceON+IPADDRESS+"/?d="+IDDEVICE+urlServiceON2);
                result = getRequest.execute(urlServiceON+IPADDRESS+"/?d="+IDDEVICE+urlServiceON2).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            //poner el estado del switch en off y el TextView con una leyenda de que no hay alarma.
            Switch sw =new Switch(STATUSOFF,context);
            sw.saveStatus();

            Alarm alarm=new Alarm(NOTHINGALARM,context);
            alarm.saveAlarm();

            notification(context);
        }
    }
    private void notification(Context context){
        Log.i("notification","notification");
        Intent notificationIntent=new Intent(context, MainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent contentIntent=PendingIntent.getActivity(context,0,notificationIntent,PendingIntent.FLAG_UPDATE_CURRENT);


        Uri defaultsound= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long [] pattern=new long[]{100,100};

        NotificationCompat.Builder builder=new NotificationCompat.Builder(context);
        builder.setAutoCancel(true)
                .setContentTitle("Vabtime")
                .setContentText("El boiler se ha prendido de acuerdo a la hora programada")
                .setContentInfo("Boiler encendido")
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.gota))
                .setSound(defaultsound)
                .setVibrate(pattern);
        notificationManager=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID,builder.build());
    }
}
