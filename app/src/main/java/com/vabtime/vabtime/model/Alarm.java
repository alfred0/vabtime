package com.vabtime.vabtime.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Alfredo on 15/02/2018.
 */

public class Alarm {
    private Context context;
    private String ALARM="alarm";
    private String CADENA_VACIA="";
    private String alarma;

    public Alarm(Context context) {
        this.context=context;
    }

    public Alarm(String alarma, Context context) {
        this.alarma=alarma;
        this.context=context;
    }

    public void saveAlarm(){
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor myEditor = myPreferences.edit();
        myEditor.putString(ALARM, alarma);
        myEditor.commit();
    }

    public String getAlarm() {
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String status = myPreferences.getString(ALARM,CADENA_VACIA);
        return status;
    }
}
