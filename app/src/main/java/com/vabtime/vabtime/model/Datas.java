package com.vabtime.vabtime.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Alfredo on 15/02/2018.
 */

public class Datas {
    private Time time;
    private Time responseTime;
    private Context context;
    private String HOUR="hour";
    private String MINUTE="minute";
    private String CADENA_VACIA="";

    public Datas(Context context) {
        this.context=context;
    }

    public Datas(Time time,Context context) {
        this.time=time;
        this.context=context;
    }

    public void saveTime(){
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor myEditor = myPreferences.edit();
        myEditor.putString(HOUR, time.getHour());
        myEditor.putString(MINUTE,time.getMinute());
        myEditor.commit();
    }

    public Time getResponseTime() {
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String hour = myPreferences.getString(HOUR,CADENA_VACIA);
        String minute = myPreferences.getString(MINUTE,CADENA_VACIA);
        responseTime=new Time();
        responseTime.setHour(hour);
        responseTime.setMinute(minute);
        return responseTime;
    }
}
