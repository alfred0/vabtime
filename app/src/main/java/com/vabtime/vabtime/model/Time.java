package com.vabtime.vabtime.model;

/**
 * Created by Alfredo on 15/02/2018.
 */

public class Time {
    private String hour;
    private String minute;

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }
}
