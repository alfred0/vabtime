package com.vabtime.vabtime.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.vabtime.vabtime.R;
import com.vabtime.vabtime.model.Device;
import com.vabtime.vabtime.sqlite.VabtimeDataBase;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class FragmentNewDevice extends Fragment implements View.OnClickListener {
    private Button anadirDsipositivo;
    private EditText ip,nombre;
    private View view;
    private Device device;
    private VabtimeDataBase vabtimeDataBase;
    private DecoratedBarcodeView dbvScanner;
    private final Handler myHandler = new Handler();




    public FragmentNewDevice() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_fragment_new_device, container, false);
        inicializarViews();
        anadirDsipositivo.setOnClickListener(this);
        device=new Device();
        vabtimeDataBase= new VabtimeDataBase(getContext());

        dbvScanner.decodeContinuous(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                updateText(result.getText());
                beepSound();
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints) {

            }
        });


        return view;
    }
    protected void beepSound() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void inicializarViews(){
        anadirDsipositivo=(Button)view.findViewById(R.id.anadirDsipositivo);
        ip=(EditText)view.findViewById(R.id.ip);
        nombre=(EditText)view.findViewById(R.id.nombre);
        dbvScanner = (DecoratedBarcodeView) view.findViewById(R.id.dbv_barcode);
    }


    public void updateText(String scanCode) {
        ip.setText(scanCode);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.anadirDsipositivo:
                if(!validarFormulario())
                    Toast.makeText(getContext(),"Datos incompletos",Toast.LENGTH_SHORT).show();
                else{
                    final ProgressDialog dialog = new ProgressDialog(getContext());
                    dialog.setTitle("Guardando...");
                    dialog.setMessage("Porfavor espere.");
                    dialog.setIndeterminate(true);
                    dialog.setCancelable(false);
                    dialog.show();

                    long delayInMillis = 5000;
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            device.setIpAdress(ip.getText().toString());
                            device.setName(nombre.getText().toString());
                            vabtimeDataBase= new VabtimeDataBase(getContext());
                            vabtimeDataBase.newDevice(device);
                            updateGUI();
                            dialog.dismiss();
                        }
                    }, delayInMillis);
                }
                break;
        }
    }
    private void updateGUI() {
        myHandler.post(myRunnable);
    }

    final Runnable myRunnable = new Runnable() {
        public void run() {
            ip.setText("");
            nombre.setText("");
        }
    };

    public boolean validarFormulario(){
        if(ip.getText().toString().equals("")|| nombre.getText().toString().equals("")){
            return false;
        }else
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeScanner();
    }

    protected void resumeScanner() {
        //isScanDone = false;
        if (!dbvScanner.isActivated())
            dbvScanner.resume();
        Log.d("peeyush-pause", "paused: false");
    }

    protected void pauseScanner() {
        dbvScanner.pause();
    }

    @Override
    public void onPause() {
        super.onPause();
        pauseScanner();
    }

    void requestPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0 && grantResults.length < 1) {
            requestPermission();
        } else {
            dbvScanner.resume();
        }
    }

}
