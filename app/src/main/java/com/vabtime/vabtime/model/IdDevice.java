package com.vabtime.vabtime.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Alfredo on 15/02/2018.
 */

public class IdDevice {
    private Context context;
    private String IDDEVICE="idDevice";
    private String CADENA_VACIA="";
    private String id;

    public IdDevice(Context context) {
        this.context=context;
    }

    public IdDevice(String id, Context context) {
        this.id=id;
        this.context=context;
    }

    public void saveIdDevice(){
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor myEditor = myPreferences.edit();
        myEditor.putString(IDDEVICE, id);
        myEditor.commit();
    }

    public String getIdDevice() {
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String status = myPreferences.getString(IDDEVICE,CADENA_VACIA);
        return status;
    }
}
