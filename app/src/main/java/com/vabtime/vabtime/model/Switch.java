package com.vabtime.vabtime.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Alfredo on 15/02/2018.
 */

public class Switch {
    private Context context;
    private String STATUS="status";
    private String CADENA_VACIA="";
    private String estado;

    public Switch(Context context) {
        this.context=context;
    }

    public Switch(String estado, Context context) {
        this.estado=estado;
        this.context=context;
    }

    public void saveStatus(){
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor myEditor = myPreferences.edit();
        myEditor.putString(STATUS, estado);
        myEditor.commit();
    }

    public String getStatus() {
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String status = myPreferences.getString(STATUS,CADENA_VACIA);
        return status;
    }
}
