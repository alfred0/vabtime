package com.vabtime.vabtime.sqlite;

import android.provider.BaseColumns;

/**
 * Created by ELVIA on 14/03/2018.
 */

public class SchemaVabtime {
    //Table devices
    public static  abstract class TABLEDEVICES implements BaseColumns {
        public static final String NAMETABLE = "devices";// Table name
        public static final String NAME = "name"; // devices name's
        public static final String IPADRESS = "ipaddress"; // devices Ip address's
        public static final String INDENTIFICATOR = "indentificator"; //devices identificator's
    }
}
