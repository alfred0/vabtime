package com.vabtime.vabtime.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Alfredo on 15/02/2018.
 */

public class IpAddress {
    private Context context;
    private String IPADDRESS="ipadd";
    private String CADENA_VACIA="";
    private String ip;

    public IpAddress(Context context) {
        this.context=context;
    }

    public IpAddress(String ip, Context context) {
        this.ip=ip;
        this.context=context;
    }

    public void saveiIpAddress(){
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor myEditor = myPreferences.edit();
        myEditor.putString(IPADDRESS, ip);
        myEditor.commit();
    }

    public String getIpAddress() {
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String status = myPreferences.getString(IPADDRESS,CADENA_VACIA);
        return status;
    }
}
