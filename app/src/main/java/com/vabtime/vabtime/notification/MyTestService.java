package com.vabtime.vabtime.notification;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Alfredo on 16/02/2018.
 */

public class MyTestService extends IntentService {
    public MyTestService() {
        super("MyTestService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //Do the task here
        Log.i("MyTestService", "Servicio ejecutandose. Recordatorios");
    }
}
